const path = require("path");
const commandLineArgPromptReader = require("./lib/command-line-arg-prompt-reader");
const displayTable = require("./lib/display-table");
const dataSetParser = require("./lib/data-set-parser");

// Step 1: Start the prompt
console.log("====  Violations Data Parser ====");

commandLineArgPromptReader.start(function(err, result) {
  if (err) {
  	console.error("User input error: ", err);
  	process.exit(err);
  }

  console.log("\nParsing Data...");

  // Step 2: Send filepath to parser to extract relevant data
  dataSetParser.parse(result.csvFilePath, function(err, data) {
  	if (err) {
  		console.error("Error occurred during file parsing: ", err);
  		process.exit(err);
  	}

    console.log("\nData Parsing Complete!\n");

  	// Step 3: Display data in Terminal
  	displayTable.print(data, function(err) {
  		if (err) {
  			console.error("displayTable: Error occurred during printing results.");
  			process.exit(err);
  		}

  		// Step 4: Exit program
  		process.exit(0);
  	});
  });
});

