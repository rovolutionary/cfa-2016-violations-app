# CFA 2016-2017 Violations count app

## Description
This is a simple Node.js command line application that takes a list of Violations (in CSV format) and returns the following metrics about the data set:

* The number of violations in each category
* Earliest Violation Date for each category
* Latest Violation Date for each category

## How to Start

To start the program, do the following steps:

* Open up a Terminal and navigate to the application directory
* Switch to the version of Node.js supported for the application by typing `nvm use`. If you do not have the version of Node.js required for this app, you will have to install it first using `nvm`: In your terminal, type `nvm install 6.2.2`
* Install the application dependencies (type `npm install`)
* Type `npm start` to kick-start the application. You will be prompted to enter the filepath to the CSV you want to analyze. If you do not specify a filepath, the application will use the `.csv` contained within the `./datasets/Violations-2012.csv` file.
* After specifing your CSV file, press the `"Enter"` key. This will begin the process and print your results to the Terminal when completed.

## Example

```bash
rohitkalkur@Rohits-Computer:~/Workspace/cfa-2016-violations-app:v6.2.2 (master)$ npm start

> cfa-2016-violations-app@1.0.0 start /Users/rohitkalkur/Workspace/cfa-2016-violations-app
> node index.js

====  Violations Data Parser ====
prompt: Enter a filepath to CSV:  (./datasets/Violations-2012.csv)

Parsing Data...

Data Parsing Complete!

Category                  Violation Count  Earliest Violation Date  Latest Violation Date
------------------------  ---------------  -----------------------  ---------------------
Garbage and Refuse        126              2012-01-03 00:00:00      2012-12-21 00:00:00
Unsanitary Conditions     83               2012-01-03 00:00:00      2012-12-19 00:00:00
Animals and Pests         180              2012-01-03 00:00:00      2012-12-28 00:00:00
Building Conditions       62               2012-01-12 00:00:00      2012-12-26 00:00:00
Vegetation                67               2012-02-01 00:00:00      2012-12-05 00:00:00
Chemical Hazards          17               2012-02-08 00:00:00      2012-12-06 00:00:00
Biohazards                7                2012-04-13 00:00:00      2012-12-18 00:00:00
Air Pollutants and Odors  2                2012-12-05 00:00:00      2012-12-19 00:00:00
Retail Food               1                2012-12-20 00:00:00      2012-12-20 00:00:00
```