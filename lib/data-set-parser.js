const fs = require("fs");
const path = require("path");
const parse = require("csv-parse");

module.exports.parse = function(csvFilepath, callback) {
	const violationCategoryData = new Map();
	const processedViolationIds = new Set();
	const csvParserStream = parse({
		delimiter: ",",
		columns: true
	});
	const absoluteFilePathCSV = path.resolve(csvFilepath);
	const inputStream = fs.createReadStream(absoluteFilePathCSV);
	inputStream.on("error", function(err) {
		callback(err);
	});

	// Pipe CSV file stream into CSV parser
	inputStream.pipe(csvParserStream);

	// Read data and aggregate
	csvParserStream.on("readable", function() {
  	let record;

  	while(record = csvParserStream.read()) {
    	const violationExists = processedViolationIds.has(record.violation_id);

    	if (!violationExists) {
    		processedViolationIds.add(record.violation_id);

    		const category = violationCategoryData.get(record.violation_category);
    		if (category) {
    			updateCategoryData(record, category);
    		}
    		else {
    			createNewCategory(violationCategoryData, record);
    		}
    	}
  	}
	});

	// Catch any errors
	csvParserStream.on("error", function(err) {
		callback(err);
	});

	// When we are done, trigger the callback with the result-set in array format
	csvParserStream.on("finish", function() {
		const violationDataIterator = violationCategoryData.values();
		const violationDataArray = Array.from(violationDataIterator);

		callback(null, violationDataArray);
	});
};

// Helper methods
function updateCategoryData(record, category) {
	category.violationCount++;

	const earliestViolationDateEpochTime = new Date(category.earliestViolationDate).getTime();
	const latestViolationDateEpochTime = new Date(category.latestViolationDate).getTime();
	const recordViolationDateEpochTime = new Date(record.violation_date).getTime();

	if (recordViolationDateEpochTime < earliestViolationDateEpochTime) {
		category.earliestViolationDate = record.violation_date;
	}
	if (recordViolationDateEpochTime > latestViolationDateEpochTime) {
		category.latestViolationDate = record.violation_date;
	}
}

function createNewCategory(violationCategoryData, record) {
	violationCategoryData.set(record.violation_category, {
		categoryName: record.violation_category,
		violationCount: 1,
		earliestViolationDate: record.violation_date,
		latestViolationDate: record.violation_date
	});
}