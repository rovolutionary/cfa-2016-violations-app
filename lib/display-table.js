const table = require("easy-table");

const CATEGORY_COLUMN = "Category";
const VIOLATION_COUNT_COLUMN = "Violation Count";
const EARLIEST_VIOLATION_DATE_COLUMN = "Earliest Violation Date";
const LATEST_VIOLATION_DATE_COLUMN = "Latest Violation Date";

module.exports.print = function(data, callback) {
	try {
		table.print(data, function(item, cell) {
		  cell(CATEGORY_COLUMN, item.categoryName);
		  cell(VIOLATION_COUNT_COLUMN, item.violationCount);
		  cell(EARLIEST_VIOLATION_DATE_COLUMN, item.earliestViolationDate);
		  cell(LATEST_VIOLATION_DATE_COLUMN, item.latestViolationDate);
		}, function(t) {
			t.sort([`${VIOLATION_COUNT_COLUMN}|des`]);
		  console.log(t.toString());
		  callback(null);
		});
	}
	catch(err) {
		callback(err);
	}
};