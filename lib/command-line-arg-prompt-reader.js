const prompt = require("prompt");

module.exports.start = function(callback) {

	prompt.start();

	const cliSchema = {
		properties: {
			csvFilePath: {
				description: "Enter a filepath to CSV",
				type: "string",
				default: "./datasets/Violations-2012.csv"
			}
		}
	};

	prompt.get(cliSchema, function(err, result) {
	  if (err) {
	  	return callback(err);
	  }
	  callback(null, result);
	});
}